<?php

namespace Drupal\afro_taste_login\Plugin\Block;

use Drupal as DrupalAlias;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Provides a 'BetterRegisterBlock' block.
 *
 * @Block(
 *  id = "better_register_block",
 *  admin_label = @Translation("Afro register block"),
 * )
 */
class AfroRegisterBlock extends BlockBase
{

  /**
   * {@inheritdoc}
   * Retrieving and displaying the registration block
   */
  public function build()
  {
    //Creating a new user entity type and assigning it to $entity variable which we will set the registration form to
    $entity = DrupalAlias::entityTypeManager()->getStorage('user')->create([]);
    //creates a formObject with which accesses the registration form and setting it to the user entity type we created
    $formObject = DrupalAlias::entityTypeManager()
      ->getFormObject('user', 'register')
      ->setEntity($entity);
    //Builds the registration form the formObject
    $form = DrupalAlias::formBuilder()->getForm($formObject);
    return $form;
  }

  /**
   * {@block}
   * @param AccountInterface $account
   * @return AccessResult
   *
   * Restricting view access to only anonymous users
   */
  protected function blockAccess(AccountInterface $account)
  {
    return AccessResult::allowedIf($account->isAnonymous());//only allow access if the current account is anonymous
  }

}
