<?php

namespace Drupal\afro_taste_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;


/**
 * Plugin implementation of the 'steps_default' formatter.
 *
 * @FieldFormatter(
 *   id = "steps_default",
 *   label = @Translation("Default"),
 *   field_types = {
 *     "steps",
 *   }
 * )
 */
class StepsDefaultFormatter extends FormatterBase {


  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {

      $step = $item->step;
      $instruction = $item->instruction;

      $elements[$delta] = [
        '#type' => 'markup',
        '#markup' => '<div class="step">Step: ' . $step . '</div><div class="instruction">' . $instruction . '</div>',
      ];
    }

    return $elements;
  }

}

