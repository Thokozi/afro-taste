<?php
namespace Drupal\afro_taste_field\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Field\WidgetInterface;

/**
* Plugin implementation of the 'steps' field type
*
* @FieldType (
 *  id = "steps",
 *  label = @Translation("Steps"),
 *  description = @Translation("Creates a step by step field"),
 *  default_widget = "steps_widget",
 *  default_formatter = "steps_default",
* )
*/

class StepsItem extends FieldItemBase {

  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    // Define the properties for the field type.
    $properties = [];

    // Define the "step" property.
    $properties['step'] = DataDefinition::create('integer')
      ->setLabel(t('Step'));

    // Define the "instruction" property.
    $properties['instruction'] = DataDefinition::create('string')
      ->setLabel(t('Instruction'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {

    return [
      'columns' => [
        'step' => [
          'type' => 'int',
          'unsigned' => TRUE,
        ],
        'instruction' => [
          'type' => 'text',
          'size' => 'big',
        ],
      ],
    ];
  }


   /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    // Check if the field item is empty.
    $step = $this->get('step')->getValue();
    $instruction = $this->get('instruction')->getValue();

    return ($step === NULL || $step === '') && ($instruction === NULL || $instruction === '');
  }

}