<?php
namespace Drupal\afro_taste_field\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Plugin implementation of the 'steps_widget' widget.
 *
 * @FieldWidget(
 *   id = "steps_widget",
 *   module = "afro_taste_field",
 *   label = @Translation("Step By Step"),
 *   field_types = {
 *     "steps"
 *   }
 * )
 */

 class StepsWidget extends WidgetBase {

  /**
   * Constructs a IngredientWidget.
   *
   * @param string $plugin_id
   *   The plugin_id for the widget.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the widget is associated.
   * @param array $settings
   *   The widget settings.
   * @param array $third_party_settings
   *   Any third party settings.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings']
    );
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element['step'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Step'),
      '#default_value' => isset($items[$delta]->step) ? $items[$delta]->step : NULL,
      '#size' => 8,
      '#maxlength' => 8,
      '#attributes' => ['class' => ['field--step']],
    ];
    $element['instruction'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Instruction'),
      '#default_value' => isset($items[$delta]->instruction) ? $items[$delta]->instruction : NULL,
      '#attributes' => ['class' => ['field--instruction']],
    ];

    $element['#element_validate'] = [[$this, 'validate']];
    $element['#attached']['library'][] = 'afro_taste_field/drupal.steps';

    return $element;
  }
  public function validate(array $element, FormStateInterface $form_state) {
    if (empty($element['instruction']['#value']) && !empty($element['step']['#value'])) {
        $form_state->setError($element['instruction'], $this->t('You must add instruction for this step'));
        return;
      }
  }

 }
