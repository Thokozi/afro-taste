<?php
/**
 * @file
 * Contains \Drupal\afro_create_recipe\Form\RecipeForm
 */
namespace Drupal\afro_create_recipe\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;

class RecipeForm extends FormBase {


  /**
   * Getter method for Form ID.
   *
   * @return string
   *   The unique ID of the form defined by this class.
   */
  public function getFormId() {
    return 'recipe_form';
  }

  /**
   * Build the simple form.
   *
   * @param array $form
   *   Default form array structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object containing current form state.
   *
   * @return array
   *   The render array defining the elements of the form.
   */

  public function buildForm(array $form, FormStateInterface $form_state) {

    /*$form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Recipe Description')
    ];

    $form['preparation'] = [
      '#type' => 'number',
      '#title' => $this->t('Preparation Time'),
    ];

    $form['cooking'] = [
      '#type' => 'number',
      '#title' => $this->t('Cooking Time'),
    ];

    $form['ingredients'] = [


    ];

    $form['instructions'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Step by step Instructions'),
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit Recipe'),
    ];*/
    return $form;
  }

  /**
   * Implements form validation.
   *
   * @param array $form
   *   The render array of the currently built form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object describing the current state of the form.
   */
  /**public function validateForm(array &$form, FormStateInterface $form_state) {

  }*/

  /**
   * Implements a form submit handler.
   *
   * @param array $form
   *   The render array of the currently built form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object describing the current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $title = $form_state->getValue('title');
    $description = $form_state->getValue('description');
    $preparation = $form_state->getValue('preparation');
    $cooking = $form_state->getValue('cooking');
    $instructions = $form_state->getValue('instructions');

    $this->CreateNode($title, $description, $preparation, $cooking, $instructions);

    $this->messenger()->addStatus($this->t('Thanks for submitting your recipe! Title: %title, Description: %description, Preparation: %preparation, Cooking: %cooking and Instructions: %instructions',
    ['%title' => $title, '%description' => $description, '%preparation' => $preparation, '%cooking' => $cooking, '%instructions' => $instructions]));
  }

  public function CreateNode ($title, $description, $preparation, $cooking, $instructions) {
    $node = Node::create(array(
      'type' => 'recipe',
      'uid' => '1',
      'langcode' => 'en',
      'status' => 1,
      'title' => $title,
      'field_recipe_description' => $description,
      'recipe_prep_time' => $preparation,
      'recipe_cook_time' => $cooking,
      'field_step_by_step_instructions' => $instructions
    ));

    $node->save();

  }
}
