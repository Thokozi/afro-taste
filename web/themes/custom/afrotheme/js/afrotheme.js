(function ($, window) {
  //Change of class name function for navigation slide-in animation
    $('.material-icons-round').click(function(){
      if($('.menu--main').hasClass('inactive')){
        $('.menu--main').removeClass('inactive').addClass('active');
      }else{
        $('.menu--main').removeClass('active').addClass('inactive');
      }
    });

    //Change of class name function for Login page fade-in animation
  $('.login__cover__wrapper__button__register').click(function(){
    console.log("here");
     if($('.login__cover').hasClass('no_slide')) {
      $('.login__cover').removeClass('no_slide').addClass('slide');
      $('.register__cover').removeClass('slide').addClass('no_slide');
    }
  });

  $('.register__cover__wrapper__button__login').click(function(){
    console.log("here");
    if($('.register__cover').hasClass('begin')){
      console.log("true");
      $('.register__cover').removeClass('begin').addClass('slide');
      $('.login__cover').removeClass('begin').addClass('no_slide');
    }if($('.register__cover').hasClass('no_slide')) {
      $('.register__cover').removeClass('no_slide').addClass('slide');
      $('.login__cover').removeClass('slide').addClass('no_slide');
    }
  });

  //parent class to create receipe button

  $('.create-recipe-button').parent().addClass('create-recipe');

  //parent class to logout button

  $('.logout-button').parent().addClass('logout');


})(jQuery, window);
